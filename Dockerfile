FROM openjdk:7-jre-alpine

COPY target /target
COPY run.sh /run.sh
RUN chmod 777 run.sh
EXPOSE 8080
CMD ["run","run.sh"]